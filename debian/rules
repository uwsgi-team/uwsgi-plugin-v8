#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright 2017 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for uWSGI V8 plugin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include /usr/share/dpkg/pkg-info.mk

# Fuse uWSGI and our package versions, flattened to native format
UWSGI_VERSION = $(shell dpkg-query --show --showformat '$${Version}' uwsgi-src)
OUR_BINARY_VERSION = $(subst -,+,$(UWSGI_VERSION))+$(DEB_VERSION)

%:
	dh $@ --with uwsgi

override_dh_auto_build:
	uwsgi --build-plugin /usr/src/uwsgi/plugins/v8
	help2man \
		--name 'fast (pure C), self-healing, developer-friendly WSGI server' \
		--section 1 \
		--no-info \
		--version-string="$(UWSGI_VERSION)" \
		debian/uwsgi_v8 \
		> debian/uwsgi_v8.1

override_dh_gencontrol:
	dh_gencontrol -- -v$(OUR_BINARY_VERSION)
